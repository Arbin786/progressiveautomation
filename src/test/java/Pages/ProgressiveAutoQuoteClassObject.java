package Pages;

import org.openqa.selenium.By; 
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.List; 


public class ProgressiveAutoQuoteClassObject {

	WebDriver driver = null;

	WebElement element = null;

	By autoOnlyOption = By.xpath("//span[contains(@class,'img')]");

	public ProgressiveAutoQuoteClassObject(WebDriver driver) {

		this.driver = driver;
	}

	public void autoOnlyOption() throws InterruptedException 
	{
		ScreenShotsClassObject.captureScreenShots(driver, "Progressive Home  Page");
		driver.findElement(autoOnlyOption).click();
		
		Thread.sleep(1000);

	}
}