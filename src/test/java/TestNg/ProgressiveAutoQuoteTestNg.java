package TestNg;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.ProgressiveAboutYourSelfClassObject;
import Pages.ProgressiveAboutYourVehicleClassObject;
import Pages.ProgressiveAutoQuoteClassObject;
import Pages.ProgressiveGetAQuoteClassObject;
import Pages.ProgressiveStartMyQuoteClassObject;

public class ProgressiveAutoQuoteTestNg {

	static WebDriver driver = null;

	@BeforeSuite

	public void beforeSuit()

	{

		System.out.println("Before Suite method");
	}

	@BeforeTest

	public void setUptest() {

		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();

	}

	public static void navigateToProgressive() {

		ChromeOptions options = new ChromeOptions();
		options.addArguments("incognito");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		options.merge(capabilities);
		driver = new ChromeDriver(options);
		driver.navigate().to("https://www.progressive.com/");
		driver.navigate().refresh();
		System.out.println(driver.getTitle() + "------------------- Web Page Launched ");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	@Test // Get a Auto Quote

	public static void ProgressiveAutoQuote() throws InterruptedException {
		// navigate to Progressive.com

		navigateToProgressive();

		// Creating Webelement Objects ProgressiveAutoQuote & Click AutoQuote

		ProgressiveAutoQuoteClassObject obj1 = new ProgressiveAutoQuoteClassObject(driver);
		Thread.sleep(1100);
		obj1.autoOnlyOption();

	}

	@Test // Input ZipCode and Get a Quote

	public static void ProgressiveGetAQuote() throws InterruptedException {
		ProgressiveGetAQuoteClassObject obj2 = new ProgressiveGetAQuoteClassObject(driver);

		obj2.enterZipCode("30033");
		obj2.getAQuote();

	}

	@Test // Input Personal Info

	public static void ProgressiveStartMyQuote() throws InterruptedException {
		ProgressiveStartMyQuoteClassObject obj3 = new ProgressiveStartMyQuoteClassObject(driver);

		obj3.validatingStartMyQuotePage();
		obj3.enterFirstName("Aayan");
		obj3.enterLastName("Shrestha");
		obj3.enterSuffix("Sr");
		obj3.enterDOB("07/12/1998");
		obj3.enterStreetName("1000 Manin St");
		obj3.enterAptNumber("1002");
		obj3.cityBox("Decatur");
		obj3.zipCodeBox("30033");
		obj3.poBoxChkBox();
		obj3.startMyQuote();

	}

	@Test // Input Vehicle Info

	public static void ArbinProgressiveAboutYourVehicle() throws InterruptedException

	{
		ProgressiveAboutYourVehicleClassObject obj4 = new ProgressiveAboutYourVehicleClassObject(driver);
		Thread.sleep(5000);
		
		obj4.selectVehicleYear("2018");
		obj4.selectVehicleMake("Audi");
		obj4.selectVehicleModel("A7");
		obj4.primaryUse("Personal");
		obj4.vehicleRideSharingBox();
		obj4.primaryZIPLocationBox("30033");
		obj4.selectOwnOrLease("Own");
		obj4.selectOwnershipDuration("1 month - 1 year");
		obj4.VehicleInfoDoneBtm();
		obj4.continueToNextPage();

	}

	@Test // Input Residency and Driving History info
	public static void ArbinProgressiveAboutYourSelf() throws InterruptedException

	{
		ProgressiveAboutYourSelfClassObject obj5 = new ProgressiveAboutYourSelfClassObject(driver);

		obj5.selectMale();
		obj5.maritalStatus("Single");
		obj5.SocialSecurityNumber("566-15-5252");
		obj5.primaryResidence("Own home");
		obj5.MovedInLast2Months("No");
		obj5.USLicenseStatus("Valid");
		obj5.yearsLicensed("3 years or more");
		obj5.chooseClaims("");
		obj5.chooseTickets("");
		obj5.continueBtm();

	}
	
	

	@AfterTest

	public static void terminateTest() throws InterruptedException 
	{

		// Close the browser
		Thread.sleep(10000);
		 driver.close();
		 driver.quit();

	}

	@AfterMethod
	@AfterSuite
	public void tearDown() {

	}

}
