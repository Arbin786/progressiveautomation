package Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import Pages.ProgressiveGetAQuoteClassObject;
import Pages.ProgressiveStartMyQuoteClassObject;
import Pages.ProgressiveAboutYourSelfClassObject;
import Pages.ProgressiveAboutYourVehicleClassObject;
import Pages.ProgressiveAutoQuoteClassObject;

public class ProgressiveAutoQuoteTest {

	private static WebDriver driver = null;

	public static void main(String[] args) throws InterruptedException {
		// step 1: Invoke Browser
		invokeBrowser();
		// Step 2 : Perform action on the web page
		getAutoQuote();
		// step 3 : Close the Browser

		closeBrowser();

	}

	public static void invokeBrowser() {
		

		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		ChromeOptions options = new ChromeOptions();
		options.addArguments("incognito");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		options.merge(capabilities);
		driver = new ChromeDriver(options);
		driver.navigate().to("https://www.progressive.com/");
		System.out.println(driver.getTitle() + "------------------- Web Page Launched ");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();

	}

	public static void getAutoQuote() throws InterruptedException {
		// Creating an object for ProgressiveAutoQuote(HomePage) & Selecting Auto Quote

		ProgressiveAutoQuoteClassObject obj1 = new ProgressiveAutoQuoteClassObject(driver);
		Thread.sleep(1100);
		obj1.autoOnlyOption();

		// Creating an object for ProgressiveGetAQuote, Entering ZiP and Click getAQuote

		ProgressiveGetAQuoteClassObject obj2 = new ProgressiveGetAQuoteClassObject(driver);

		obj2.enterZipCode("30033");
		obj2.getAQuote();

		// Creating an object for ProgressiveStartMyQuote, Entering ZiP and Click
		// getAQuote

		ProgressiveStartMyQuoteClassObject obj3 = new ProgressiveStartMyQuoteClassObject(driver);

		obj3.validatingStartMyQuotePage();
		obj3.enterFirstName("Aayan");
		obj3.enterLastName("Shrestha");
		obj3.enterSuffix("Sr");
		obj3.enterDOB("07/12/1998");
		obj3.enterStreetName("1000 Manin St");
		obj3.enterAptNumber("1002");
		obj3.cityBox("Decatur");
		obj3.zipCodeBox("30033");
		obj3.poBoxChkBox();
		obj3.startMyQuote();

		// Creating an object for AboutYourVehiclePage, Entering Vehicle Info

		ProgressiveAboutYourVehicleClassObject obj4 = new ProgressiveAboutYourVehicleClassObject(driver);

		
		obj4.selectVehicleYear("2018");
		obj4.selectVehicleMake("Audi");
		obj4.selectVehicleModel("A7");
		// obj4.bodyType("4DR 6CYL");
		obj4.primaryUse("Personal");
		obj4.vehicleRideSharingBox();
		obj4.primaryZIPLocationBox("30033");
		obj4.selectOwnOrLease("Own");
		obj4.selectOwnershipDuration("1 month - 1 year");
		obj4.VehicleInfoDoneBtm();
		obj4.continueToNextPage();

		// Creating an object for AboutYourSelfPage, Entering Your Info

		ProgressiveAboutYourSelfClassObject obj5 = new ProgressiveAboutYourSelfClassObject(driver);
		Thread.sleep(5000);
		// obj5.AboutYourSelfPage();
		obj5.selectMale();
		obj5.maritalStatus("Single");
		obj5.SocialSecurityNumber("566-15-5252");
		obj5.primaryResidence("Own home");
		obj5.MovedInLast2Months("No");
		obj5.USLicenseStatus("Valid");
		obj5.yearsLicensed("3 years or more");
		obj5.chooseClaims("");
		obj5.chooseTickets("");
		obj5.continueBtm();

	}

	public static void closeBrowser() throws InterruptedException {
		// Close the browser
		Thread.sleep(1000);
		driver.close();
		driver.quit();

	}
}





















