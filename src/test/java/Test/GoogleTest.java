package Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import Pages.GoogleSearchClassObject;


public class GoogleTest {

	private static WebDriver driver = null;

	public static void main(String[] args) throws InterruptedException {
		// step 1: Invoke Browser
		invokeBrowser();
		// Step 2 : Perform action on the web page
		googleSearch();
		// step 3 : Close the Browser

		closeBrowser();

	}

	public static void invokeBrowser() {

		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		ChromeOptions options = new ChromeOptions();
		options.addArguments("incognito");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		options.merge(capabilities);
		driver = new ChromeDriver(options);
		driver.navigate().to("https://www.google.com/");
		System.out.println(driver.getTitle() + "------------------- Web Page Launched ");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();

	}

	public static void googleSearch() throws InterruptedException {
		// Creating an object for ProgressiveAutoQuote(HomePage) & Selecting Auto Quote

		GoogleSearchClassObject obj1 = new GoogleSearchClassObject(driver);
		obj1.googleSearch("What is TestNG");
		

	}

	public static void closeBrowser() throws InterruptedException {
		// Close the browser
		Thread.sleep(2000);
		driver.close();
		driver.quit();

	}
}
