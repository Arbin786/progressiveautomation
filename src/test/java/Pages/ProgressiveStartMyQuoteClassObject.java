package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ProgressiveStartMyQuoteClassObject {

	WebDriver driver = null;

	WebElement element = null;

	By validatingStartMyQuotePage = By.xpath(
			"//span[@class='persuasion-header' and  text()='Your neighbors love us and we think you will too!']");

	By enterFirstName = By.xpath("//input[contains(@id,'NameAndAddressEdit_embedded_questions_list_FirstName')]");

	By enterLastName = By.xpath("//input[contains(@id,'NameAndAddressEdit_embedded_questions_list_LastName')]");

	By enterSuffix = By.xpath("//select [contains(@analytics-id, 'NameAndAddressEdit_Suffix' )]");

	By enterDOB = By.xpath(
			"//input[@id='NameAndAddressEdit_embedded_questions_list_DateOfBirth' and @name = 'NameAndAddressEdit_embedded_questions_list_DateOfBirth']");

	By enterStreetName = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_MailingAddress']");

	By enterAptNumber = By.xpath("//input[contains(@id,'NameAndAddressEdit_embedded_questions_list_ApartmentUnit')]");

	By cityBox = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_City' and @type = 'text']");

	By zipCodeBox = By.xpath("(//input[contains(@type,'tel')])[2]");

	By poBoxChkBox = By.xpath("//input[contains(@type,'checkbox')]");

	By startMyQuote = By.xpath("//button[contains(text(),'Okay, start my quote.')]");

	public ProgressiveStartMyQuoteClassObject(WebDriver driver) {

		this.driver = driver;
	}

	public void validatingStartMyQuotePage() {
		WebElement validate = driver.findElement(validatingStartMyQuotePage);

		if (validate.getText().contains("Your neighbors love us and we think you will too!")) {
			System.out.println(
					"PASS--Expected Message ---'Your neighbors love us and we think you will too!'-- is displayed as expected");
		}

		else {
			System.out.println("Fail");

		}

	}

	public void enterFirstName(String FirstName) throws InterruptedException {

		driver.findElement(enterFirstName).sendKeys(FirstName);
		Thread.sleep(1500);

	}

	public void enterLastName(String LastName) throws InterruptedException {

		driver.findElement(enterLastName).sendKeys(LastName);
		Thread.sleep(1200);
	}

	public void enterSuffix(String suff) throws InterruptedException {

		WebElement suffix = driver.findElement(enterSuffix);

		if (suffix.getText().contains(suff)) {

			suffix.sendKeys(suff);
			Thread.sleep(1100);

		}

	}

	public void enterDOB(String DOB) throws InterruptedException {
		driver.findElement(enterDOB).sendKeys(DOB);
		Thread.sleep(1100);
		driver.findElement(enterDOB).sendKeys(Keys.TAB);

	}

	public void enterStreetName(String streetName) throws InterruptedException {

		Thread.sleep(1100);
		driver.findElement(enterStreetName).sendKeys(streetName);
		Thread.sleep(1100);

	}

	public void enterAptNumber(String aptNumber) throws InterruptedException {

		driver.findElement(enterAptNumber).sendKeys(aptNumber);
		Thread.sleep(1100);

	}

	public void cityBox(String City) throws InterruptedException {

		WebElement enteredCity = driver.findElement(cityBox);
		if (enteredCity.getText().contains(City)) {

			System.out.println("PASS--Expected  ------City is displayed as expected");
			Thread.sleep(1200);

		} else {
			System.out.println("Matching City Failed");
			driver.findElement(cityBox).clear();

			driver.findElement(cityBox).sendKeys(City);

			System.out.println("Input City Displayed-------- ");

		}
	}

	public void zipCodeBox(String ZipCode) throws InterruptedException {

		WebElement enteredZip = driver.findElement(zipCodeBox);
		if (enteredZip.getText().contains(ZipCode)) {

			System.out.println("PASS--Expected  ------ZipCode is displayed as expected");
			Thread.sleep(1200);

		} else {
			System.out.println("Matchin Zip Failed");
			driver.findElement(zipCodeBox).clear();

			driver.findElement(zipCodeBox).sendKeys(ZipCode);

			System.out.println("Input ZipCode Displayed-------- ");
		}

	}

	public void poBoxChkBox() throws InterruptedException {

		WebElement poBoxMilitaryAddress = driver.findElement(poBoxChkBox);

		if (poBoxMilitaryAddress.isSelected()) {
			System.out.println("P.O. Box/Military Address Checkbox  is Toggled On");

		} else {
			System.out.println("P.O. Box/Military Address Checkbox is Toggled Off");
			poBoxMilitaryAddress.click();
		}

	}

	public void startMyQuote() throws InterruptedException {

		driver.findElement(startMyQuote).click();
		ScreenShotsClassObject.captureScreenShots(driver, "StartMyQuote Page");
		Thread.sleep(1100);

	}

}
